package com.madiyar.project.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

    @Controller
    public class MainController {
        @GetMapping("/")
        public String home() {
            return "home";
        }
        @GetMapping("/about")
        public String about() {
            return "about";
        }
        @GetMapping("/cars")
        public String cars() {
            return "cars";
        }
        @GetMapping("/service")
        public String service() {
            return "service";
        }
        @GetMapping("/contact")
        public String contact() {
            return "contact";
        }
        @GetMapping("/blog")
        public String blog() {
            return "blog";
        }
        @GetMapping("/team")
        public String team() {
            return "team";
        }
}
